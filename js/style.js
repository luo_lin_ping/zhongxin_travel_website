$('.right li').eq(4).mouseenter(function(){
	$(this).css({'background-color':'#fff','height':'100px'});
	$(this).children().css('display','block');
});

$('.right li').eq(4).mouseleave(function(){
	$(this).css({'background-color':'','height':''});
	$(this).children().css('display','none');
});

// 北京

$('.address_box').mouseenter(function(){
	$('.address_m').css('display','block');
});

$('.address_m').mouseleave(function(){
	$(this).css('display','none');
});


// 输入框

$('.search_box').click(function(){
	$('.search_more').css('display','block');
});

$('.search_more').mouseleave(function(){
	$(this).css('display','none');
});


// 高级搜索
$('.senior').mouseenter(function(){
	$('.senior_m').css('display','block');
});

$('.senior').mouseleave(function(){

	$('.senior_m').css('display','none');
});

$('.more_on').click(function(){
	$('.left1').css({'height':'auto','overflow':'visible'});
	$('.l_hide').css('display','block');
	$(this).css('display','none');
});

$('.l_hide').click(function(){
	$('.left1').css({'height':'26px','overflow':'hidden'});
	$('.more_on').css('display','block');
});


// nav

$('a').mouseenter(function(){
	$('.nav_f').css({'width':'1142px','left':'0px','display':'block'})
});

$('a').mouseenter(function(){
	$('.nav_f').css({'width':'','left':'','display':'none'});
});


// 侧边栏
	

$('.pic span').mouseenter(function(){
	var index=$(this).index();
	 $('.no').eq(index).css('width','100px').stop().animate({'left':'-100px'}, 50);
});

$('.pic2 span').mouseenter(function(){
	 $('.no').css('width','100px').stop().animate({'left':'-100px'}, 50);
});

$('.no').mouseleave(function(){
	 $(this).css('width','1px').stop().animate({'left':'0px'}, 50);
});

// select1

// 1 div 更多
$('.checkBox .more').click(function(){
	$('.one dd').css({'height':'auto','overflow':'visible'});
	$('.checkBox .more1').css('display','block');
	$(this).css('display','none');
	$('.one .ensure').css('display','none');
});

// main 收起
$('.checkBox .more1').click(function(){
	$('.one dd').css({'height':'36px','overflow':'hidden'});
	$('.checkBox .more').css('display','block');
	$(this).css('display','none');
});

//  选择更多
$('.one .checkMore').click(function(){
	$('.one dd').css({'border-radius':'6px','height':'112px','overflow':'visible','background-color':'#f9f9f9'});
	$('.one dd ul').css('background-color','#f9f9f9');
	$('.one dd li').css({'border-color':'#f9f9f9','background-color':'#f9f9f9'});
	$('.one dd li span').css({'background-image':'url("../images/check-no.png")','background-repeat':'no-repeat','background-position':'6px center','padding-left':'24px','background-color':'#f9f9f9'});
	$('.one .ensure').css({'background-color':'#f9f9f9','display':'block'});
	$('.one .checkBox').css('display','none');

});

//  取消

$('.ensure .cancel').click(function(){
	$('.one dd').css({'height':'36px','overflow':'hidden','background-color':'#fff'});
	$('.one dd ul').css('background-color','#fff');
	$('.one dd li').css({'border-color':'#fff','background-color':'#fff'});
	$('.one dd li span').css({'background':'#fff','line-height':'32px','height':'34px','display':'block','padding-left':'0px'});
	$('.one .checkBox').css('display','block');
	// $('.one .checkMore').css('display','block');
	$('.one .ensure').css('display','none');

});

$('.one dd li span').click(function(){
	$(this).css('background-image','url("../images/checked1.png")');
});



// 
//2 div  选择更多
$('.checkMore1').click(function(){
	$('.two dd').css({'border-radius':'6px','height':'76px','overflow':'visible','background-color':'#f9f9f9'});
	$('.two dd ul').css('background-color','#f9f9f9');
	$('.two dd li').css({'border-color':'#f9f9f9','background-color':'#f9f9f9'});
	$('.two dd li span').css({'background-image':'url("../images/check-no.png")','background-repeat':'no-repeat','background-position':'6px center','padding-left':'24px','background-color':'#f9f9f9'});
	$('.two .ensure1').css({'background-color':'#f9f9f9','display':'block'});
	$(this).css('display','none');
});

$('.two dd li span').click(function(){
	$(this).css('background-image','url("../images/checked1.png")');
});

$('.ensure1 .cancel').click(function(){
	$('.two dd').css({'height':'36px','overflow':'hidden','background-color':'#fff'});
	$('.two dd ul').css('background-color','#fff');
	$('.two dd li').css({'border-color':'#fff','background-color':'#fff'});
	$('.two dd li span').css({'background':'#fff','line-height':'32px','height':'34px','display':'block','padding-left':'0px'});
	$('.two .checkMore1').css('display','block');
	$(this).css('display','none');
	$('.sure1').css('display','none');
});


//3 div 选择更多
$('.checkMore2').click(function(){
	$('.three dd').css({'border-radius':'6px','height':'76px','overflow':'visible','background-color':'#f9f9f9'});
	$('.three dd ul').css({'width':'330px','background-color':'#f9f9f9'});
	$('.three dd li').css({'border-color':'#f9f9f9','background-color':'#f9f9f9'});
	$('.three dd li span').css({'background-image':'url("../images/check-no.png")','background-repeat':'no-repeat','background-position':'6px center','padding-left':'24px','background-color':'#f9f9f9'});
	$('.three .ensure2').css({'background-color':'#f9f9f9','display':'block'});
	$(this).css('display','none');
	$('.data-wrap').css('display','none');

});

$('.three dd li span').click(function(){
	$(this).css('background-image','url("../images/checked1.png")');
});

// 取消

$('.ensure2 .cancel').click(function(){
	$('.three dd').css({'height':'36px','overflow':'hidden','background-color':'#fff'});
	$('.three dd ul').css('background-color','#fff');
	$('.three dd li').css({'border-color':'#fff','background-color':'#fff'});
	$('.three dd li span').css({'background':'#fff','line-height':'32px','height':'34px','display':'block','padding-left':'0px'});
	$('.three .checkMore2').css('display','block');
	$('.three .ensure2').css('display','none');
	$('.data-wrap').css('display','block');
});


// main


$('.main_top_l input').click(function(){
	$(this).parent().css('color','#ea1a56');
});


// 换一换

$('.like_top_r span').click(function(){
	$('.like_m').css('display','none');
	$('.like_m1').css('display','block');
	$('.like_top').css('display','none');
	$('.like_top1').css('display','block');
});

$('.like_top_r1 span').click(function(){
	$('.like_m1').css('display','none');
	$('.like_m').css('display','block');
	$('.like_top1').css('display','none');
	$('.like_top').css('display','block');
});

// 友情链接

$('.fr_more').click(function(){
	$('.fr_right').css({'height':'auto','overflow':'visible','width':'1070'});
	$('.hide1').css('display','block');
	$(this).css('display','none');
});

$('.hide1').click(function(){
	$('.fr_right').css({'height':'14px','width':'760','overflow':'hidden'});
	$('.fr_more').css('display','block');
});





